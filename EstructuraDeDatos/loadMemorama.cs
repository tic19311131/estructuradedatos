﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{
    public partial class loadMemorama : Form
    {
        public loadMemorama()
        {
            InitializeComponent();
        }
        SoundPlayer sonido = new SoundPlayer();

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1) this.Opacity += 0.05;
            progressBar1.Value += 1;
            if (progressBar1.Value == 100)
            {
                timer1.Stop();
                timer2.Start();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.Opacity -= 0.1;
            if (this.Opacity == 0)
            {
                timer2.Stop();
                this.Close();
                sonido.Stop();
                frmMemorama memorama = new frmMemorama();
                memorama.Show();
                this.Hide();
            }
        }

        private void loadMemorama_Load(object sender, EventArgs e)
        {
            timer1.Start();
            sonido.SoundLocation = "C:/Users/planc/Desktop/EstructuraDeDatos/Practica1/EstructuraDeDatos/EstructuraDeDatos/Resources/song/cancion.wav";
            sonido.Play();
        }
    }
}
