﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{
    public partial class FrmMatrizInversa : Form
    {
        public FrmMatrizInversa()
        {
            InitializeComponent();
        }


        public void generarMatriz()
        {
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            dataGridView3.DataSource = null;
            dataGridView4.DataSource = null;
            dataGridView5.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView2.Columns.Clear();
            dataGridView3.Columns.Clear();
            dataGridView4.Columns.Clear();
            dataGridView5.Columns.Clear();
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
            dataGridView3.Rows.Clear();
            dataGridView4.Rows.Clear();
            dataGridView5.Rows.Clear();

            for (int i = 0; i < int.Parse(txtColumnasRenglones.Text); i++)
            {
                dataGridView1.Columns.Add("Column", "Column");
                dataGridView2.Columns.Add("Column", "Column");
                dataGridView3.Columns.Add("Column", "Column");
                dataGridView4.Columns.Add("Column", "Column");
                dataGridView5.Columns.Add("Column", "Column");
            }
            for (int i = 0; i < int.Parse(txtColumnasRenglones.Text); i++)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].HeaderCell.Value = "Row" + i;
                dataGridView2.Rows.Add();
                dataGridView2.Rows[i].HeaderCell.Value = "Row" + i;
                dataGridView3.Rows.Add();
                dataGridView3.Rows[i].HeaderCell.Value = "Row" + i;
                dataGridView4.Rows.Add();
                dataGridView4.Rows[i].HeaderCell.Value = "Row" + i;
                dataGridView5.Rows.Add();
                dataGridView5.Rows[i].HeaderCell.Value = "Row" + i;

            }
            for (int r = 0; r < int.Parse(txtColumnasRenglones.Text); r++)
            {
                for (int c = 0; c < int.Parse(txtColumnasRenglones.Text); c++)
                {
                    dataGridView1[c, r].Value = "1";
                    dataGridView2[c, r].Value = "1";
                    dataGridView3[c, r].Value = "1";
                    dataGridView4[c, r].Value = "1";
                    dataGridView5[c, r].Value = "1";
                }
            }
        }
    }
}


            
        

     
