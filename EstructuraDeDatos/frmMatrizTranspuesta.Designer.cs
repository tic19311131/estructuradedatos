﻿namespace EstructuraDeDatos
{
    partial class frmMatrizTranspuesta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRenglones = new System.Windows.Forms.TextBox();
            this.txtColumnas = new System.Windows.Forms.TextBox();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.btnRanbom = new System.Windows.Forms.Button();
            this.btnTranspuesta = new System.Windows.Forms.Button();
            this.txtLimSuperior = new System.Windows.Forms.TextBox();
            this.txtLimInferior = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(48, 195);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "M = ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(48, 447);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 37);
            this.label2.TabIndex = 1;
            this.label2.Text = "M =";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(132, 108);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(704, 237);
            this.dataGridView1.TabIndex = 2;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ControlDark;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(132, 351);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(704, 237);
            this.dataGridView2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(41, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 37);
            this.label3.TabIndex = 4;
            this.label3.Text = "Renglones";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(47, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 37);
            this.label4.TabIndex = 5;
            this.label4.Text = "Columnas";
            // 
            // txtRenglones
            // 
            this.txtRenglones.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRenglones.Location = new System.Drawing.Point(167, 16);
            this.txtRenglones.Name = "txtRenglones";
            this.txtRenglones.Size = new System.Drawing.Size(100, 37);
            this.txtRenglones.TabIndex = 6;
            // 
            // txtColumnas
            // 
            this.txtColumnas.Font = new System.Drawing.Font("Myanmar Text", 12F);
            this.txtColumnas.Location = new System.Drawing.Point(167, 64);
            this.txtColumnas.Name = "txtColumnas";
            this.txtColumnas.Size = new System.Drawing.Size(100, 37);
            this.txtColumnas.TabIndex = 7;
            // 
            // btnGenerar
            // 
            this.btnGenerar.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnGenerar.Location = new System.Drawing.Point(581, 24);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(113, 35);
            this.btnGenerar.TabIndex = 8;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // btnRanbom
            // 
            this.btnRanbom.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnRanbom.Location = new System.Drawing.Point(700, 24);
            this.btnRanbom.Name = "btnRanbom";
            this.btnRanbom.Size = new System.Drawing.Size(113, 35);
            this.btnRanbom.TabIndex = 9;
            this.btnRanbom.Text = "Ranbom";
            this.btnRanbom.UseVisualStyleBackColor = true;
            this.btnRanbom.Click += new System.EventHandler(this.btnRanbom_Click);
            // 
            // btnTranspuesta
            // 
            this.btnTranspuesta.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.btnTranspuesta.Location = new System.Drawing.Point(629, 65);
            this.btnTranspuesta.Name = "btnTranspuesta";
            this.btnTranspuesta.Size = new System.Drawing.Size(147, 35);
            this.btnTranspuesta.TabIndex = 10;
            this.btnTranspuesta.Text = "Transpuesta";
            this.btnTranspuesta.UseVisualStyleBackColor = true;
            this.btnTranspuesta.Click += new System.EventHandler(this.btnTranspuesta_Click);
            // 
            // txtLimSuperior
            // 
            this.txtLimSuperior.Font = new System.Drawing.Font("Myanmar Text", 12F);
            this.txtLimSuperior.Location = new System.Drawing.Point(439, 56);
            this.txtLimSuperior.Name = "txtLimSuperior";
            this.txtLimSuperior.Size = new System.Drawing.Size(100, 37);
            this.txtLimSuperior.TabIndex = 14;
            this.txtLimSuperior.Text = "100";
            // 
            // txtLimInferior
            // 
            this.txtLimInferior.Font = new System.Drawing.Font("Myanmar Text", 12F);
            this.txtLimInferior.Location = new System.Drawing.Point(439, 12);
            this.txtLimInferior.Name = "txtLimInferior";
            this.txtLimInferior.Size = new System.Drawing.Size(100, 37);
            this.txtLimInferior.TabIndex = 13;
            this.txtLimInferior.Text = "-100";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(274, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(168, 37);
            this.label5.TabIndex = 12;
            this.label5.Text = "Limite Superior";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(284, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 37);
            this.label6.TabIndex = 11;
            this.label6.Text = "Limite Inferior";
            // 
            // frmMatrizTranspuesta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(848, 605);
            this.Controls.Add(this.txtLimSuperior);
            this.Controls.Add(this.txtLimInferior);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnTranspuesta);
            this.Controls.Add(this.btnRanbom);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.txtColumnas);
            this.Controls.Add(this.txtRenglones);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmMatrizTranspuesta";
            this.Text = "frmMatrizTranspuesta";
            this.Load += new System.EventHandler(this.frmMatrizTranspuesta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRenglones;
        private System.Windows.Forms.TextBox txtColumnas;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Button btnRanbom;
        private System.Windows.Forms.Button btnTranspuesta;
        private System.Windows.Forms.TextBox txtLimSuperior;
        private System.Windows.Forms.TextBox txtLimInferior;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}