﻿namespace EstructuraDeDatos
{
    partial class FrmPilaGenerica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPilaGenerica));
            this.lbl10Elementos = new System.Windows.Forms.Label();
            this.lblTope = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnElementAt = new System.Windows.Forms.Button();
            this.btnReversa = new System.Windows.Forms.Button();
            this.btnRandom = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnContains = new System.Windows.Forms.Button();
            this.btnGet = new System.Windows.Forms.Button();
            this.btnPilita = new System.Windows.Forms.Button();
            this.btnPop = new System.Windows.Forms.Button();
            this.txtInferior = new System.Windows.Forms.TextBox();
            this.txtSuperior = new System.Windows.Forms.TextBox();
            this.txtCaptura = new System.Windows.Forms.TextBox();
            this.lblPila = new System.Windows.Forms.Label();
            this.txtIndice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Pila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl10Elementos
            // 
            this.lbl10Elementos.AutoSize = true;
            this.lbl10Elementos.Location = new System.Drawing.Point(12, 20);
            this.lbl10Elementos.Name = "lbl10Elementos";
            this.lbl10Elementos.Size = new System.Drawing.Size(148, 13);
            this.lbl10Elementos.TabIndex = 0;
            this.lbl10Elementos.Text = "La pila contiene 10 elementos";
            // 
            // lblTope
            // 
            this.lblTope.AutoSize = true;
            this.lblTope.Location = new System.Drawing.Point(12, 51);
            this.lblTope.Name = "lblTope";
            this.lblTope.Size = new System.Drawing.Size(32, 13);
            this.lblTope.TabIndex = 1;
            this.lblTope.Text = "Tope";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Pila});
            this.dataGridView1.Location = new System.Drawing.Point(15, 67);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(210, 390);
            this.dataGridView1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(257, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Limite inferior";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(257, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Limite superior";
            // 
            // btnElementAt
            // 
            this.btnElementAt.Location = new System.Drawing.Point(547, 130);
            this.btnElementAt.Name = "btnElementAt";
            this.btnElementAt.Size = new System.Drawing.Size(138, 30);
            this.btnElementAt.TabIndex = 5;
            this.btnElementAt.Text = "pilita.ElementAt()";
            this.btnElementAt.UseVisualStyleBackColor = true;
            this.btnElementAt.Click += new System.EventHandler(this.btnElementAt_Click);
            // 
            // btnReversa
            // 
            this.btnReversa.Location = new System.Drawing.Point(547, 203);
            this.btnReversa.Name = "btnReversa";
            this.btnReversa.Size = new System.Drawing.Size(138, 30);
            this.btnReversa.TabIndex = 6;
            this.btnReversa.Text = "pilita.Reverse()";
            this.btnReversa.UseVisualStyleBackColor = true;
            this.btnReversa.Click += new System.EventHandler(this.btnReversa_Click);
            // 
            // btnRandom
            // 
            this.btnRandom.Location = new System.Drawing.Point(242, 295);
            this.btnRandom.Name = "btnRandom";
            this.btnRandom.Size = new System.Drawing.Size(138, 30);
            this.btnRandom.TabIndex = 7;
            this.btnRandom.Text = "Random";
            this.btnRandom.UseVisualStyleBackColor = true;
            this.btnRandom.Click += new System.EventHandler(this.btnRandom_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(386, 295);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(138, 30);
            this.btnLimpiar.TabIndex = 8;
            this.btnLimpiar.Text = "pilita.Clear()";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnContains
            // 
            this.btnContains.Location = new System.Drawing.Point(530, 295);
            this.btnContains.Name = "btnContains";
            this.btnContains.Size = new System.Drawing.Size(138, 30);
            this.btnContains.TabIndex = 9;
            this.btnContains.Text = "pilita.Contains()";
            this.btnContains.UseVisualStyleBackColor = true;
            this.btnContains.Click += new System.EventHandler(this.btnContains_Click);
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(674, 295);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(138, 30);
            this.btnGet.TabIndex = 10;
            this.btnGet.Text = "pilita.getType()";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // btnPilita
            // 
            this.btnPilita.Location = new System.Drawing.Point(242, 362);
            this.btnPilita.Name = "btnPilita";
            this.btnPilita.Size = new System.Drawing.Size(138, 30);
            this.btnPilita.TabIndex = 11;
            this.btnPilita.Text = "pilita.Push()";
            this.btnPilita.UseVisualStyleBackColor = true;
            // 
            // btnPop
            // 
            this.btnPop.Location = new System.Drawing.Point(242, 425);
            this.btnPop.Name = "btnPop";
            this.btnPop.Size = new System.Drawing.Size(138, 30);
            this.btnPop.TabIndex = 12;
            this.btnPop.Text = "pilita.Pop";
            this.btnPop.UseVisualStyleBackColor = true;
            this.btnPop.Click += new System.EventHandler(this.btnPop_Click);
            // 
            // txtInferior
            // 
            this.txtInferior.Location = new System.Drawing.Point(337, 147);
            this.txtInferior.Name = "txtInferior";
            this.txtInferior.Size = new System.Drawing.Size(161, 20);
            this.txtInferior.TabIndex = 13;
            this.txtInferior.Text = "1";
            // 
            // txtSuperior
            // 
            this.txtSuperior.Location = new System.Drawing.Point(337, 200);
            this.txtSuperior.Name = "txtSuperior";
            this.txtSuperior.Size = new System.Drawing.Size(161, 20);
            this.txtSuperior.TabIndex = 14;
            this.txtSuperior.Text = "100";
            // 
            // txtCaptura
            // 
            this.txtCaptura.Location = new System.Drawing.Point(487, 368);
            this.txtCaptura.Name = "txtCaptura";
            this.txtCaptura.Size = new System.Drawing.Size(198, 20);
            this.txtCaptura.TabIndex = 15;
            this.txtCaptura.TextChanged += new System.EventHandler(this.txtCaptura_TextChanged);
            // 
            // lblPila
            // 
            this.lblPila.AutoSize = true;
            this.lblPila.Location = new System.Drawing.Point(257, 60);
            this.lblPila.Name = "lblPila";
            this.lblPila.Size = new System.Drawing.Size(123, 13);
            this.lblPila.TabIndex = 16;
            this.lblPila.Text = "En el tope de la pila hay:";
            // 
            // txtIndice
            // 
            this.txtIndice.Location = new System.Drawing.Point(487, 425);
            this.txtIndice.Name = "txtIndice";
            this.txtIndice.Size = new System.Drawing.Size(201, 20);
            this.txtIndice.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(426, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Captura";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(434, 428);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Indice";
            // 
            // Pila
            // 
            this.Pila.HeaderText = "Pilas";
            this.Pila.Name = "Pila";
            this.Pila.Width = 174;
            // 
            // FrmPilaGenerica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 493);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIndice);
            this.Controls.Add(this.lblPila);
            this.Controls.Add(this.txtCaptura);
            this.Controls.Add(this.txtSuperior);
            this.Controls.Add(this.txtInferior);
            this.Controls.Add(this.btnPop);
            this.Controls.Add(this.btnPilita);
            this.Controls.Add(this.btnGet);
            this.Controls.Add(this.btnContains);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnRandom);
            this.Controls.Add(this.btnReversa);
            this.Controls.Add(this.btnElementAt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lblTope);
            this.Controls.Add(this.lbl10Elementos);
            this.Name = "FrmPilaGenerica";
            this.Text = resources.GetString("$this.Text");
            this.Load += new System.EventHandler(this.FrmPilaGenerica_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl10Elementos;
        private System.Windows.Forms.Label lblTope;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnElementAt;
        private System.Windows.Forms.Button btnReversa;
        private System.Windows.Forms.Button btnRandom;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnContains;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Button btnPilita;
        private System.Windows.Forms.Button btnPop;
        private System.Windows.Forms.TextBox txtInferior;
        private System.Windows.Forms.TextBox txtSuperior;
        private System.Windows.Forms.TextBox txtCaptura;
        private System.Windows.Forms.Label lblPila;
        private System.Windows.Forms.TextBox txtIndice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pila;
    }
}