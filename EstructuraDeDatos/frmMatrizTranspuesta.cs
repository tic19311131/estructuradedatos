﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{

    public partial class frmMatrizTranspuesta : Form
    {
        Random random = new Random();
        public frmMatrizTranspuesta()
        {
            InitializeComponent();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear();
            dataGridView2.Columns.Clear();
            for (int i = 0; i < int.Parse(txtColumnas.Text); i++)
            {
                dataGridView1.Columns.Add("Colum" + i, "Colum" + i);
            }

            for (int i = 0; i < int.Parse(txtRenglones.Text); i++)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[i].HeaderCell.Value = "Row" + i;
            }

            for (int i = 0; i < int.Parse(txtRenglones.Text); i++)
            {
                dataGridView2.Columns.Add("Colum" + i, "Colum" + i);
            }

            for (int i = 0; i < int.Parse(txtColumnas.Text); i++)
            {
                dataGridView2.Rows.Add();
                dataGridView2.Rows[i].HeaderCell.Value = "Row" + i;
            }
        }

        private void btnRanbom_Click(object sender, EventArgs e)
        {
            for (int r = 0; r < int.Parse(txtRenglones.Text); r++)
            {
                for (int c = 0; c < int.Parse(txtColumnas.Text); c++)
                {

                    dataGridView1[c, r].Value = random.Next(int.Parse(txtLimInferior.Text) - 1, int.Parse(txtLimSuperior.Text) + 1);

                }
            }

        }

        private void btnTranspuesta_Click(object sender, EventArgs e)
        {
            for (int r = 0; r < int.Parse(txtRenglones.Text); r++)
            {
                for (int c = 0; c < int.Parse(txtColumnas.Text); c++)
                {
                    dataGridView2[r, c].Value = dataGridView1[c, r].Value.ToString();

                }
            }
        }

        private void frmMatrizTranspuesta_Load(object sender, EventArgs e)
        {

        }
    }
}
