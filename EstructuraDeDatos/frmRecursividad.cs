﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EstructuraDeDatos
{
    public partial class frmRecursividad : Form
    {
        ulong a = 0;
        ulong b = 1;
        ulong c = 0;
        public frmRecursividad()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dgLimites.Rows.Clear();
            a = 0;
            b = 1;
            c = 0;
            dgLimites.Rows.Add("0");
            dgLimites.Rows[0].HeaderCell.Value = "1";

            for (int i = 0; i < 1000; i++)
            {
                if (c >= ulong.Parse(textBox1.Text))
                {
                    dgLimites.Rows.Add(c.ToString());
                    dgLimites.Rows[i].HeaderCell.Value = (i+1).ToString();
                    c = a + b;
                    a = b;
                    b = c;


                }
            }
        }
    }
}
